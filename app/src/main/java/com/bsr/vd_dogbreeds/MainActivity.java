package com.bsr.vd_dogbreeds;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.bsr.vd_dogbreeds.model.BreedImagesResponse;
import com.bsr.vd_dogbreeds.model.BreedListModelResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button mButtonMoreDogs;
    private ImageView mImageViewDog;
    private Spinner mSpinnerBreeds;
    private ArrayAdapter<String> mBreedsListAdapter;
    private BreedListModelResponse mBreedListResponse;
    private BreedImagesResponse mOnlineBreedImagesResponse;
    private BreedImagesResponse mOfflineBreedImagesResponse;


    private DBOperations mDbOperations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDbOperations = new DBOperations(this);

        mSpinnerBreeds = findViewById(R.id.spinner_breeds);
        mBreedsListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item);
        mSpinnerBreeds.setAdapter(mBreedsListAdapter);
        mSpinnerBreeds.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mImageViewDog.setVisibility(View.INVISIBLE);
                mButtonMoreDogs.setVisibility(View.INVISIBLE);
                if (isNetworkAvailable()) {
                    getImagesByBreed();
                } else {
                    getOfflineImagesByBreed();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mImageViewDog = findViewById(R.id.image_view_dog);

        mButtonMoreDogs = findViewById(R.id.button_more_dogs);
        mButtonMoreDogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    if (mOnlineBreedImagesResponse != null && mOnlineBreedImagesResponse.currentBreed != null && mOnlineBreedImagesResponse.currentBreed.equals(mSpinnerBreeds.getSelectedItem().toString())
                            && mOnlineBreedImagesResponse.message != null && mOnlineBreedImagesResponse.message.size() > 0) {
                        if (!hasStoragePermission() && mOnlineBreedImagesResponse.positionInList == 0) {
                            Toast.makeText(MainActivity.this, "Storage permission is not enabled. You can't view all these images in offline mode", Toast.LENGTH_SHORT).show();
                        }
                        mOnlineBreedImagesResponse.positionInList++;
                        if (mOnlineBreedImagesResponse.positionInList >= mOnlineBreedImagesResponse.message.size()) {
                            mOnlineBreedImagesResponse.positionInList = 0;
                        }
                        setOnlineImage();
                    } else {
                        mImageViewDog.setVisibility(View.INVISIBLE);
                        mButtonMoreDogs.setVisibility(View.INVISIBLE);
                        getImagesByBreed();
                    }
                } else {
                    if (mOfflineBreedImagesResponse != null && mOfflineBreedImagesResponse.currentBreed != null && mOfflineBreedImagesResponse.currentBreed.equals(mSpinnerBreeds.getSelectedItem().toString())
                            && mOfflineBreedImagesResponse.message != null && mOfflineBreedImagesResponse.message.size() > 0) {
                        mOfflineBreedImagesResponse.positionInList++;
                        if (mOfflineBreedImagesResponse.positionInList >= mOfflineBreedImagesResponse.message.size()) {
                            mOfflineBreedImagesResponse.positionInList = 0;
                        }
                        setOfflineImage();
                    } else {
                        mImageViewDog.setVisibility(View.INVISIBLE);
                        mButtonMoreDogs.setVisibility(View.INVISIBLE);
                        getOfflineImagesByBreed();
                    }
                }
            }
        });

        if (hasStoragePermission()) {
            getBreeds();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        }
    }

    private void getBreeds() {
        if (isNetworkAvailable()) {
            getBreedList();
        } else {
            getOfflineBreeds();
        }
    }

    private void getBreedList() {
        String url = "https://dog.ceo/api/breeds/list/all";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        List<String> breedsSpinner = new ArrayList<>();
                        mBreedListResponse = new Gson().fromJson(response, BreedListModelResponse.class);
                        if (mBreedListResponse.message != null) {
                            mDbOperations.open();
                            List<Integer> breedNameHashList = new ArrayList<>();
                            Cursor cursor = mDbOperations.getBreedNameHashCode();
                            if (cursor != null) {
                                while (cursor.moveToNext()) {
                                    breedNameHashList.add(cursor.getInt(0));
                                }
                            }
                            for (Field field : mBreedListResponse.message.getClass().getDeclaredFields()) {
                                if (!field.isSynthetic() && !(field.getType() == Long.TYPE)) {
                                    breedsSpinner.add(field.getName());
                                    if (!breedNameHashList.contains(field.getName().hashCode())) {
                                        mDbOperations.insert(field.getName(), field.getName().hashCode(), new Gson().toJson(new ArrayList<Integer>()),
                                                new Gson().toJson(new ArrayList<String>()), 0);
                                    }
                                }
                            }
                            mDbOperations.close();
                        }
                        mBreedsListAdapter.addAll(breedsSpinner);
                        mBreedsListAdapter.notifyDataSetChanged();
                        if (breedsSpinner.size() > 0) {
                            mSpinnerBreeds.setSelection(0);
                        } else {
                            showToast();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error Get Breeds: ", error.getMessage());
                    }
                }
        );

        VDApplication.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void getOfflineBreeds() {
        mDbOperations.open();
        List<String> breedsSpinner = new ArrayList<>();
        Cursor cursor = mDbOperations.getBreedNameList();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                breedsSpinner.add(cursor.getString(cursor.getColumnIndex(BreedListContract.BreedEntry.COLUMN_NAME_BREED_NAME)));
            }
        }
        mDbOperations.close();
        mBreedsListAdapter.addAll(breedsSpinner);
        mBreedsListAdapter.notifyDataSetChanged();
        if (breedsSpinner.size() > 0) {
            mSpinnerBreeds.setSelection(0);
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setTitle("Network Connection Error")
                    .setMessage("No data available. Make sure Network connection is ON and open application again. Thanks.")
                    .setPositiveButton("Close Application", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.this.finish();
                        }
                    })
                    .create();
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }

    private void getImagesByBreed() {
        String url = String.format("https://dog.ceo/api/breed/%s/images", mSpinnerBreeds.getSelectedItem().toString());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mOnlineBreedImagesResponse = new Gson().fromJson(response, BreedImagesResponse.class);
                        if (mOnlineBreedImagesResponse.message != null && mOnlineBreedImagesResponse.message.size() > 0) {
                            mImageViewDog.setVisibility(View.VISIBLE);
                            mButtonMoreDogs.setVisibility(View.VISIBLE);
                            mOnlineBreedImagesResponse.positionInList = 0;
                            mOnlineBreedImagesResponse.currentBreed = mSpinnerBreeds.getSelectedItem().toString();
                            setOnlineImage();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error BreedImages: ", error.getMessage());
                    }
                }
        );
        VDApplication.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void setOnlineImage() {
        final String url = mOnlineBreedImagesResponse.message.get(mOnlineBreedImagesResponse.positionInList);
        ImageRequest imageRequest = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        mImageViewDog.setImageBitmap(response);

                        File pictureFile = null;
                        if (hasStoragePermission()) {
                            pictureFile = getOutputMediaFile();
                        }
                        if (pictureFile == null) {
                            Log.d("fdf", "Error creating media file.");
                            return;
                        }

                        mDbOperations.open();
                        Cursor cursor = mDbOperations.getImagesHashAndPathsByBreed(mSpinnerBreeds.getSelectedItem().toString());
                        if (cursor != null) {
                            cursor.moveToFirst();
                            String breedImageHashcodesJson = cursor.getString(0);
                            ArrayList<Integer> breedImageHashcodes = new Gson().fromJson(breedImageHashcodesJson, new TypeToken<ArrayList<Integer>>() {
                            }.getType());
                            if (!breedImageHashcodes.contains(url.hashCode())) {
                                try {
                                    FileOutputStream fos = new FileOutputStream(pictureFile);
                                    response.compress(Bitmap.CompressFormat.PNG, 90, fos);
                                    fos.close();

                                    breedImageHashcodes.add(url.hashCode());
                                    String breedImagePathsJson = cursor.getString(1);
                                    ArrayList<String> breedImagePaths = new Gson().fromJson(breedImagePathsJson, new TypeToken<ArrayList<String>>() {
                                    }.getType());
                                    breedImagePaths.add(pictureFile.getAbsolutePath());
                                    if (mOfflineBreedImagesResponse != null && mOnlineBreedImagesResponse != null
                                            && mOfflineBreedImagesResponse.currentBreed != null && mOnlineBreedImagesResponse.currentBreed != null
                                            && mOfflineBreedImagesResponse.currentBreed.equals(mOnlineBreedImagesResponse.currentBreed)) {
                                        mOfflineBreedImagesResponse.message = breedImagePaths;
                                    }
                                    mDbOperations.update(mSpinnerBreeds.getSelectedItem().toString(),
                                            new Gson().toJson(breedImageHashcodes), new Gson().toJson(breedImagePaths), mOnlineBreedImagesResponse.message.size());

                                } catch (FileNotFoundException e) {
                                    Log.d("vd", "File not found: " + e.getMessage());
                                } catch (IOException e) {
                                    Log.d("vd", "Error accessing file: " + e.getMessage());
                                }
                            }
                        }
                        mDbOperations.close();
                    }
                },
                0, 0, ImageView.ScaleType.FIT_XY, null,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        VDApplication.getInstance(this.getApplicationContext()).addToRequestQueue(imageRequest);
//        mImageViewDog.setImageUrl(mOnlineBreedImagesResponse.message.get(mOnlineBreedImagesResponse.positionInList),
//                VDApplication.getInstance(this.getApplicationContext()).getImageLoader());
    }

    private void getOfflineImagesByBreed() {
        mDbOperations.open();
        Cursor cursor = mDbOperations.getImagesByBreed(mSpinnerBreeds.getSelectedItem().toString());
        if (cursor != null) {
            cursor.moveToFirst();
            String breedImagePathsJson = cursor.getString(0);
            ArrayList<String> breedImagePaths = new Gson().fromJson(breedImagePathsJson, new TypeToken<ArrayList<String>>() {
            }.getType());
            if (breedImagePaths.size() > 0) {
                mOfflineBreedImagesResponse = new BreedImagesResponse();
                mOfflineBreedImagesResponse.message = breedImagePaths;
                mOfflineBreedImagesResponse.currentBreed = mSpinnerBreeds.getSelectedItem().toString();
                mImageViewDog.setVisibility(View.VISIBLE);
                mButtonMoreDogs.setVisibility(View.VISIBLE);
                setOfflineImage();
            } else {
                showToast();
            }
        }
        mDbOperations.close();
    }

    private void setOfflineImage() {
        String imagePath = mOfflineBreedImagesResponse.message.get(mOfflineBreedImagesResponse.positionInList);
        File imgFile = new File(imagePath);
        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            mImageViewDog.setImageBitmap(myBitmap);
        } else {
            showToast();
        }
    }


    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files/VirtualDk");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        File mediaFile;
        String mImageName = "VD_" + Calendar.getInstance().getTimeInMillis() + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null;
    }

    private void showToast() {
        Toast.makeText(this, "Please go online", Toast.LENGTH_SHORT).show();
    }

    private boolean hasStoragePermission() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestpermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setMessage("Storage permission is required to view the application in offline mode.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getBreeds();
                            Toast.makeText(MainActivity.this, "Storage Permission denied. Enable permission in settings to view images in offline mode", Toast.LENGTH_LONG).show();
                        }
                    })
                    .create();
            alertDialog.setCancelable(false);
            alertDialog.show();
        } else {
            getBreeds();
            Toast.makeText(MainActivity.this, "Storage Permission denied. Enable permission in settings to view images in offline mode", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 101) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getBreeds();
            } else {
                requestpermission();
            }
        }
    }

}
