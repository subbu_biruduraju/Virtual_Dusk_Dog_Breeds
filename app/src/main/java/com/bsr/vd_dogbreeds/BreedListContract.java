package com.bsr.vd_dogbreeds;

import android.provider.BaseColumns;

public final class BreedListContract {

    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + BreedEntry.TABLE_NAME + " (" +
                    BreedEntry._ID + " INTEGER PRIMARY KEY," +
                    BreedEntry.COLUMN_NAME_BREED_NAME + " TEXT," +
                    BreedEntry.COLUMN_NAME_BREED_NAME_HASHCODE + " INTEGER," +
                    BreedEntry.COLUMN_NAME_BREED_IMAGE_HASHCODE_LIST + " TEXT," +
                    BreedEntry.COLUMN_NAME_BREED_IMAGE_PATH_LIST + " TEXT," +
                    BreedEntry.COLUMN_NAME_SERVER_BREED_IMAGES_SIZE + " INTEGER)";

    static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + BreedEntry.TABLE_NAME;

    private BreedListContract() {
    }

    public static class BreedEntry implements BaseColumns {
        public static final String TABLE_NAME = "breedEntries";
        public static final String COLUMN_NAME_BREED_NAME = "breedName";
        public static final String COLUMN_NAME_BREED_NAME_HASHCODE = "breedNameHashCode";
        public static final String COLUMN_NAME_BREED_IMAGE_HASHCODE_LIST = "breedImageHashCodeList";
        public static final String COLUMN_NAME_BREED_IMAGE_PATH_LIST = "breedImagePathList";
        public static final String COLUMN_NAME_SERVER_BREED_IMAGES_SIZE = "serverImagesSize";
    }

}
