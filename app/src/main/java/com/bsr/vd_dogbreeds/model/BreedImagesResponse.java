package com.bsr.vd_dogbreeds.model;

import java.util.List;

public class BreedImagesResponse {
    public String status;
    public List<String> message = null;
    public transient int positionInList;
    public transient String currentBreed;
}
