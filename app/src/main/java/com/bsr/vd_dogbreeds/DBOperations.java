package com.bsr.vd_dogbreeds;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBOperations {

    private BreedListDBHelper mDBHelper;
    private SQLiteDatabase mSQLiteDatabase;

    public DBOperations(Context context) {
        mDBHelper = new BreedListDBHelper(context);
    }

    public void open() {
        mSQLiteDatabase = mDBHelper.getWritableDatabase();
    }

    public void insert(String breedName, int hashCode, String imageHashcodeList, String breedImagePathList, int breedSize) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BreedListContract.BreedEntry.COLUMN_NAME_BREED_NAME, breedName);
        contentValues.put(BreedListContract.BreedEntry.COLUMN_NAME_BREED_NAME_HASHCODE, hashCode);
        contentValues.put(BreedListContract.BreedEntry.COLUMN_NAME_BREED_IMAGE_HASHCODE_LIST, imageHashcodeList);
        contentValues.put(BreedListContract.BreedEntry.COLUMN_NAME_BREED_IMAGE_PATH_LIST, breedImagePathList);
        contentValues.put(BreedListContract.BreedEntry.COLUMN_NAME_SERVER_BREED_IMAGES_SIZE, breedSize);
        mSQLiteDatabase.insert(BreedListContract.BreedEntry.TABLE_NAME, null, contentValues);
    }

    public void update(String breedName, String breedImageHashList, String breedImagePathList, int breedSize) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BreedListContract.BreedEntry.COLUMN_NAME_BREED_IMAGE_HASHCODE_LIST, breedImageHashList);
        contentValues.put(BreedListContract.BreedEntry.COLUMN_NAME_BREED_IMAGE_PATH_LIST, breedImagePathList);
        contentValues.put(BreedListContract.BreedEntry.COLUMN_NAME_SERVER_BREED_IMAGES_SIZE, breedSize);
        mSQLiteDatabase.update(BreedListContract.BreedEntry.TABLE_NAME, contentValues,
                BreedListContract.BreedEntry.COLUMN_NAME_BREED_NAME + "=?", new String[]{breedName});
    }

    public void delete(String id){
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        db.delete("ordertable","_id=?",new String[] {id}); //delete all rows in a table
    }

    public Cursor getBreedNameList(){
        return mSQLiteDatabase.query(BreedListContract.BreedEntry.TABLE_NAME,
                new String[]{BreedListContract.BreedEntry.COLUMN_NAME_BREED_NAME},
                null,null,null,null,null);
    }

    public Cursor getBreedNameHashCode() {
        return mSQLiteDatabase.query(BreedListContract.BreedEntry.TABLE_NAME,
                new String[]{BreedListContract.BreedEntry.COLUMN_NAME_BREED_NAME_HASHCODE},
                null,null,null,null,null);
    }

    public Cursor getImagesByBreed(String breedName) {
        return mSQLiteDatabase.query(BreedListContract.BreedEntry.TABLE_NAME,
                new String[]{BreedListContract.BreedEntry.COLUMN_NAME_BREED_IMAGE_PATH_LIST, BreedListContract.BreedEntry.COLUMN_NAME_SERVER_BREED_IMAGES_SIZE},
                BreedListContract.BreedEntry.COLUMN_NAME_BREED_NAME + "=?", new String[]{breedName},
                null, null, null);
    }

    public Cursor getImagesHashAndPathsByBreed(String breedName) {
        return mSQLiteDatabase.query(BreedListContract.BreedEntry.TABLE_NAME,
                new String[]{BreedListContract.BreedEntry.COLUMN_NAME_BREED_IMAGE_HASHCODE_LIST, BreedListContract.BreedEntry.COLUMN_NAME_BREED_IMAGE_PATH_LIST},
                BreedListContract.BreedEntry.COLUMN_NAME_BREED_NAME + "=?", new String[]{breedName},
                null, null, null);
    }

    public void close(){
        mSQLiteDatabase.close();
    }
}
